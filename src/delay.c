#include <delay.h>


// Init timer for Microseconds delays
void _init_us() {
	// Enable clock for TIM2
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
	// Time base configuration
	
	TIM2->PSC = (SystemCoreClock/DELAY_TIM_FREQUENCY_US)-1;
	TIM2->ARR = UINT16_MAX;
	TIM2->CR1 &= ~TIM_CR1_CKD;
	TIM2->CR1 &= ~ TIM_CR1_DIR;

	// Enable counter for TIM2
	TIM2->CR1 |= TIM_CR1_CEN;
}

// Init and start timer for Milliseconds delays
void _init_ms() {
	// Enable clock for TIM2
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;

	// Time base configuration
	TIM2->PSC = (SystemCoreClock/DELAY_TIM_FREQUENCY_MS)-1;
	TIM2->ARR = UINT16_MAX;
	TIM2->CR1 &= ~TIM_CR1_CKD;
	TIM2->CR1 &= ~TIM_CR1_DIR;

	// Enable counter for TIM2
	TIM2->CR1 |= TIM_CR1_CEN;
}

// Stop timer
void _stop_timer() {
	TIM2->CR1 &= ~TIM_CR1_CEN;
	RCC->APB1ENR &= ~RCC_APB1ENR_TIM2EN; // Powersavings?
}

// Do delay for nTime milliseconds
void Delay_ms(uint32_t mSecs) {
	// Init and start timer
	_init_ms();

	// Dummy loop with 16 bit count wrap around
	volatile uint32_t start = TIM2->CNT;
	while((TIM2->CNT-start) <= mSecs);

	// Stop timer
	_stop_timer();
}

// Do delay for nTime microseconds
void Delay_us(uint32_t uSecs) {
	// Init and start timer
	_init_us();

	// Dummy loop with 16 bit count wrap around
	volatile uint32_t start = TIM2->CNT;
	while((TIM2->CNT-start) <= uSecs);

	// Stop timer
	_stop_timer();
}
